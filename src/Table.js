import React from 'react';
import MaterialTable from 'material-table';
import Icons from '@material-ui/core/Icon';

function Table(props) {

    let filtering = false
    if (props.applyOptions) {
        filtering = props.options.filtering ? true : false
    }

    let options = {
        //actionsColumnIndex: -1,
        headerStyle: {
            backgroundColor: '#fad732',
            color: '#000',
            position: 'relative',
            zIndex: '0',
            fontSize: '90%',
            textAlign: 'center',
            padding: '10px 10px 10px 10px'
        },
        cellStyle: {
            textAlign: 'center'
        },
        pageSize: 5,
        filtering,
        grouping: false,
        exportButton: false
    };
    
    const localization = {
        pagination: {
            labelDisplayedRows: '{from}-{to} of {count}',
            labelRowsPerPage: 'Rows per pages:',
            labelRowsSelect: 'Rows',
            firstAriaLabel: 'First',
            firstTooltip: 'First',
            previousAriaLabel: 'Previous',
            previousTooltip: 'Previous',
            nextAriaLabel: 'Next',
            nextTooltip: 'Next',
            lastAriaLabel: 'Lastest',
            lastTooltip: 'Lastest',
        },
        toolbar: {
            nRowsSelected: '{0} row(s) selected',
            searchTooltip: 'Search',
            searchPlaceholder: 'Search'
        },
        header: {
            actions: 'Actions'
        },
        body: {
            filterRow:{
                filterTooltip: 'Filter'
            },
            addTooltip: 'New',
            deleteTooltip: 'Delete',
            editTooltip: 'Edit',
            editRow: {
                cancelTooltip: 'Cancel',
                saveToolTip: 'Save',
                deleteText: 'Are you sure you want to delete the record?'           
            },
            emptyDataSourceMessage: 'No records found'
        },
        grouping: {
            placeholder: "Drag headings here to group by "
        }
    };

    if(props.applyOptions){
        options.exportButton = (props.options.exportButton!==undefined&&props.options.exportButton!==null?props.options.exportButton:true);
        options.grouping = (props.options.grouping!==undefined&&props.options.grouping!==null?props.options.grouping:false);        
        options.selection = (props.options.selection!==undefined&&props.options.selection!==null?props.options.selection:false);
        options.search = (props.options.search!==undefined&&props.options.search!==null?props.options.search:true);
        if(props.options.fixedColumns!==undefined&&props.options.fixedColumns!==null)
            options.fixedColumns = {
                left: (props.options.fixedColumns.left!==undefined&&props.options.fixedColumns.left!==null?props.options.fixedColumns.left:0),
                right: (props.options.fixedColumns.right!==undefined&&props.options.fixedColumns.right!==null?props.options.fixedColumns.right:0)
            }
        options.paging =props.options.paging !== null ? props.options.paging : true
    } 

    return (
        <MaterialTable
            Icons={Icons}
            title={props.titleTable}
            columns={props.columns}
            data={props.data}
            editable={props.editable}
            localization={localization}
            options={options}
            actions={props.actions}
            onSelectionChange={props.onSelectionChange}
            cellEditable={props.cellEditable}
        />
    )
}

export default Table;